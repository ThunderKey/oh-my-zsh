git-sync() {
  git pull && git push
}
alias gsync="git-sync"

git-detect-default-branch() {
  branches=($(git branch --format='%(refname:short)'))
  for b in master main; do
    if [[ $branches =~ (^|[[:space:]])$b($|[[:space:]]) ]]; then
      echo $b
      return 0
    fi
  done

  echo "No default branch found"
  return 1
}

git-cleanup() {
  branch=$1
  [[ "$branch" == "" ]] && branch=$(git-detect-default-branch)
  if [[ "$branch" != "stay" ]]; then
    git checkout $branch || return $?
  fi
  git fetch -p || return $?
  git pull || return $?
  branches=($(git branch -vv | awk '/: (entfernt|gone)]/{print $1}'))
  if [[ "$branches" == "" ]]; then
    echo "No unnecessary branches"
    return 0
  fi
  for branch in $branches; do
    git branch -d $branch
  done
}
alias gcleanup="git-cleanup"

git-to-push() {
  branch=$(git rev-parse --abbrev-ref HEAD)
  git log "origin/$branch".."$branch"
}
alias gtp=git-to-push

alias gdto='git difftool'
alias gss='git status --short'

alias greptex="grep -r --include='*.tex'"
alias o=xdg-open

alias tx=tmuxinator

alias_if() {
  which $2 &> /dev/null && alias $1=$2
}
alias_if todo todo-txt
alias_if t todo_txt_viewer
alias_if dw dirwatch
alias_if bat batcat

if which nvim &> /dev/null; then
  alias v=nvim
elif which vim &> /dev/null; then
  alias v=$(which vim)
else
  echo "no vim found..."
fi
alias vim="echo use v"
alias vdiff="v -d"
alias vdiffis="vdiff -c 'set diffopt+=iwhite'"

alias cal="cal -w -3"

lmk-init() {
  echo "
\$pdflatex = 'xelatex -interaction=nonstopmode -halt-on-error -shell-escape';
\$out_dir = 'out';

\$max_repeat = 10;

\$pdf_previewer = 'xdg-open %O %S';

add_cus_dep( 'acn', 'acr', 0, 'makeglossaries' );
add_cus_dep( 'glo', 'gls', 0, 'makeglossaries' );
add_cus_dep( 'recordtype-glo', 'recordtype-gls', 0, 'makeglossaries' );
\$clean_ext .= ' acr acn alg glo gls glg recordtype-gls recordtype-glo recordtype-glg';
sub makeglossaries {
  my (\$name, \$path) = fileparse( \$\$Psource );
  return system \"makeglossaries -d '\$path' '\$name'\";
}" > .latexmkrc
}

alias lmk="latexmk -pdf -pvc"
alias lmkq="lmk -quiet"
alias ri="rubber-info --into out *.tex"

only-real-files() {
  while read line; do
    [ -f $line ] && echo $line
  done < /dev/stdin
}

# git ag
gag() {
  ag "$@" $(git ls-files | only-real-files)
}
ggrep() {
  grep "$@" $(git ls-files | only-real-files)
}

TODO_SEARCH="TODO|TBD|FIXME|XXX"
ftodo() {
  fag --word "$TODO_SEARCH"
}
fgtodo() {
  fgag --word "$TODO_SEARCH"
}
