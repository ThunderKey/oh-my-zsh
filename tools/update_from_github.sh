#!/bin/bash

pushd $ZSH > /dev/null

abort() {
  echo -e "\e[31m$2\e[0m" >&2

  exit $1
}

git remote | grep github > /dev/null || git remote add github https://github.com/ohmyzsh/ohmyzsh.git

git pull || abort 1 'could not pull'
git pull github master --rebase=false || abort 1 'update unsuccessful'

grep github tools/install.sh | grep -v 'https://github.com/zkat/supports-hyperlinks\|https://gist.github.com/XVilka/8346728' > /dev/null && abort 2 'The word github appeared in the tools/install.sh file'
grep github.com README.md | grep -v wiki | grep -v issues | grep -v fonts | grep -v 'shields\.io' | grep -v 'huntr\.dev' | grep -ve 'github.com/$\?{.*}' | grep -v github.com:user/project && abort 2 'The word github appeared in the README.md file'

git push origin master
echo updated successfuly
[ -e ~/tmp/oh-my-zsh_status ] && rm ~/tmp/oh-my-zsh_status
popd > /dev/null
