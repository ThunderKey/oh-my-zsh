vman() {
  if [ $# -eq 0 ]; then
    echo "What manual page do you want?";
    return 0
  elif ! man -d "$@" &> /dev/null; then
    # Check that manpage exists to prevent visual noise.
    echo "No manual entry for $*"
    return 1
  fi

  $EDITOR -c "OpenMan $@"
}
compdef vman="man"
