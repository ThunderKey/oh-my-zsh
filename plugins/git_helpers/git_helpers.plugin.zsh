# load _git completion helpers
_git

 _gv () {
   local curcontext=$curcontext state line ret=1
   _alternative 'modified-files::__git_ignore_line_inside_arguments __git_modified_files' 'other-files::__git_ignore_line_inside_arguments __git_other_files' && ret=0
   return ret
 }
gv() { v "$@" }
compdef _gv gv
