# configuration:
if [[ $rails_root = "" ]]; then
  export rails_root=/var/www/rails_apps
fi
if [[ $dev_root = "" ]]; then
  export dev_root=$rails_root/dev
fi
if [[ $prod_root = "" ]]; then
  export prod_root=$rails_root/prod
fi

rails_env_info() {
  if [[ `pwd` == $dev_root* ]] ; then
    echo "%{$fg_bold[yellow]%}(dev)"
  else
    if [[ `pwd` == $prod_root* ]] ; then
      echo "%{$fg_bold[red]%}(prod)"
    fi
  fi
}

cdecho() {
  cd $1 && pwd
}

cdprod() {
  if [[ $# -eq 0 ]] ; then
    if [[ `pwd` == $dev_root* ]] ; then
      cdecho `pwd | sed -e "s|$dev_root|$prod_root|"`
    else
      echo "not in a development folder" 1>&2
    fi
  else
    cdecho $prod_root/$1
  fi
}
_cdprod() { _files -W $prod_root -/; }
compdef _cdprod cdprod

cddev() {
  if [[ $# -eq 0 ]] ; then
    if [[ `pwd` == $prod_root* ]] ; then
      cdecho `pwd | sed -e "s|$prod_root|$dev_root|"`
    else
      echo "not in a production folder" 1>&2
    fi
  else
    cdecho $dev_root/$1
  fi
}
_cddev() { _files -W $dev_root -/; }
compdef _cddev cddev

cdenv() {
  if [[ `pwd` == $prod_root* ]] ; then
    cddev
  elif [[ `pwd` == $dev_root* ]] ; then
    cdprod
  else
    echo "not in a rails project folder" 1>&2
  fi
}
