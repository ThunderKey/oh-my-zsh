## rails_helpers

This plugin helps rails developer with the following shortcuts:
* `cdprod XYZ` change the directory to the rails project XYZ within /var/www/rails_apps/prod
* `cdprod` switch to the current project but within /var/www/rails_apps/prod
* `cddev XYZ` change the directory to the rails project XYZ within /var/www/rails_apps/dev
* `cddev` switch to the current project but within /var/www/rails_apps/dev
* `cdenv` toggle the environment. If you are in the dev folder switch to the prod folder and inversely
* `railsgrep` recursivly grep for anything within the current rails folder but ignore the folders vendor and log
* `railsegrep` recursivly egrep for anything within the current rails folder but ignore the folders vendor and log

### Configuration
You can specify the following variables before the plugin gets included:
* `$rails_root` The directory where your rails apps are stored (default: /var/www/rails_apps)
* `$prod_root` The directory where your production projects are stored (default: $rails_root/prod)
* `$dev_root` The directory where your development projects are stored (default: $rails_root/dev)
