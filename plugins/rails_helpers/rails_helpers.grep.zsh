RAILSGREPOPTIONS=("--recursive")
for dir in vendor log tmp coverage node_modules packs packs-test; do
  RAILSGREPOPTIONS=($RAILSGREPOPTIONS "--exclude-dir=$dir")
done
for file in tags; do
  RAILSGREPOPTIONS=($RAILSGREPOPTIONS "--exclude=$file")
done

railsgrep() { grep $RAILSGREPOPTIONS "$@" }
railsegrep() { egrep $RAILSGREPOPTIONS "$@" }
railsfgrep() { fgrep $RAILSGREPOPTIONS "$@" }
