# fz/j => use z with fzf
# fzz/jj => use z with fzf and the default value is the last arg
# fe => edit a file with fzf
# fv => edit a file from .viminfo with fzf
# fd => cd to selected directory
# fda => including hidden directories
# fdr => cd to selected parent directory
# fkill => kill a process with fzf
# fh => get back a history entry with fzf
# fgrep => grep recursively and open the file with fzf
# fco => git checkout with fzf
# fshow => git show with fzf
# fstash => git stash with fzf (enter => show; ctrl+d => diff; ctrl+b => new branch)
# fgst => git status with fzf (ctrl+d => diff; ctrl+a => add; ctrl+p => patch; ctrl+r => reset)
# fda => select a docker container to start and attach to
# fds => select a docker container to stop
# gi => create .gitignore from gitignore.io

fz() {
  local folder
  if [[ -z "$*" ]]; then
    folder="$(_z -l 2>&1 | fzf +s --tac | sed 's/^[0-9,.]* *//')"
  else
    _last_z_args="$@"
    folder="$(_z -e "$@")"
  fi
  [[ "$folder" == "" ]] && return
  echo $folder
  cd $folder
}

fzz() {
  local folder
  folder="$(_z -l 2>&1 | sed 's/^[0-9,.]* *//' | fzf -q "$_last_z_args")"
  [[ "$folder" == "" ]] && return
  echo $folder
  cd $folder
}
alias j=fz
alias jj=fzz

fe() {
  local files
  local cmd
  IFS=$'\n' files=($(fzf-tmux --query="$1" --multi --select-1 --exit-0))
  if [[ -n "$files" ]]; then
    print -S "${EDITOR:-vim} ${files[@]}" # write the cmd in the history
    ${EDITOR:-vim} ${files[@]}
  fi
}

fv() {
  local files
  files=$(grep '^>' ~/.viminfo | cut -c3- |
          while read line; do
            [ -f "${line/\~/$HOME}" ] && echo "$line"
          done | fzf-tmux -d -m -q "$*" -1) && $EDITOR ${files//\~/$HOME}
}

fd() {
  local dir
  dir=$(find ${1:-.} -path '*/\.*' -prune \
                  -o -type d -print 2> /dev/null | fzf +m) &&
  cd "$dir"
}

fda() {
  local dir
  dir=$(find ${1:-.} -type d 2> /dev/null | fzf +m) && cd "$dir"
}

fdr() {
  local declare dirs=()
  get_parent_dirs() {
    if [[ -d "${1}" ]]; then dirs+=("$1"); else return; fi
    if [[ "${1}" == '/' ]]; then
      for _dir in "${dirs[@]}"; do echo $_dir; done
    else
      get_parent_dirs $(dirname "$1")
    fi
  }
  local DIR=$(get_parent_dirs $(realpath "${1:-$PWD}") | fzf-tmux --tac)
  cd "$DIR"
}

fcd() {
  if [[ "$#" != 0 ]]; then
    cd "$@";
    return
  fi
  while true; do
    local lsd=$(echo ".." && ls -p | grep '/$' | sed 's;/$;;')
    local dir="$(printf '%s\n' "${lsd[@]}" |
      fzf --reverse --preview '
        __cd_nxt="$(echo {})";
        __cd_path="$(echo $(pwd)/${__cd_nxt} | sed "s;//;/;")";
        echo $__cd_path;
        echo;
        ls -p --color=always "${__cd_path}";
    ')"
    [[ ${#dir} != 0 ]] || return 0
    builtin cd "$dir" &> /dev/null
  done
}

fkill() {
  local pid
  if [ "$UID" != "0" ]; then
      pid=$(ps -f -u $UID | sed 1d | fzf -m | awk '{print $2}')
  else
      pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}')
  fi

  if [ "x$pid" != "x" ]
  then
      echo $pid | xargs kill -${1:-9}
      echo killed $pid
  fi
}

fh() {
  local args
  args=""
  [[ "$@" != "" ]] && args="--query=$@"
  print -z $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) | fzf +s --tac $args | sed 's/ *[0-9]* *//')
}

fsearch() {
  local fileAndLine

  fileAndLine=$("$@" | fzf -0 -1 --ansi | awk -F: '{print $1":"$2}')
  if [[ "$fileAndLine" != "" ]]; then
    print -S "${EDITOR:-vim} $fileAndLine" # write the cmd in the history
    ${EDITOR:-vim} $fileAndLine
  fi
}

unalias fgrep
fgrep() { fsearch grep --color=always --line-number --recursive "$@" }
fag() { fsearch ag --color "$@" }
fgag() { fsearch gag --color "$@" }

fco() {
  local commits commit
  commits=$(git log --pretty=oneline --abbrev-commit --reverse) &&
  commit=$(echo "$commits" | fzf --tac +s +m -e) &&
  git checkout $(echo "$commit" | sed "s/ .*//")
}

fshow() {
  git log --graph --color=always \
      --format="%C(auto)%h%d %s %C(black)%C(bold)%cr" "$@" |
  fzf --ansi --no-sort --reverse --tiebreak=index --bind=ctrl-s:toggle-sort \
      --bind "ctrl-m:execute:
                (grep -o '[a-f0-9]\{7\}' | head -1 |
                xargs -I % sh -c 'git show --color=always % | less -R') << 'FZF-EOF'
                {}
FZF-EOF"
}

# fstash - easier way to deal with stashes
# type fstash to get a list of your stashes
# enter shows you the contents of the stash
# ctrl-d shows a diff of the stash against your current HEAD
# ctrl-b checks the stash out as a branch, for easier merging
fstash() {
  local out q k sha
  while out=$(
    git stash list --pretty="%C(yellow)%h %>(14)%Cgreen%cr %C(blue)%gs" |
    fzf --ansi --no-sort --query="$q" --print-query \
        --expect=ctrl-d,ctrl-b);
  do
    mapfile -t out <<< "$out"
    q="${out[0]}"
    k="${out[1]}"
    sha="${out[-1]}"
    sha="${sha%% *}"
    [[ -z "$sha" ]] && continue
    if [[ "$k" == 'ctrl-d' ]]; then
      git diff $sha
    elif [[ "$k" == 'ctrl-b' ]]; then
      git stash branch "stash-$sha" $sha
      break;
    else
      git stash show -p $sha
    fi
  done
}

fgst() {
  local selection control file
  while true; do
    selection=$(git -c color.status=always status --short | sort | fzf --ansi --expect=ctrl-d,ctrl-a,ctrl-p,ctrl-r,ctrl-e | tr '\n' ' ')
    if [[ "$selection" == "" ]]; then
      git status
      return
    fi
    control=$(echo $selection | awk '{print $1}')
    file=$(echo $selection | awk '{print $3}')
    if [[ "$control" == "ctrl-d" ]]; then
      GIT_PAGER='less -+F' git diff $file
    elif [[ "$control" == "ctrl-a" ]]; then
      git add $file
    elif [[ "$control" == "ctrl-p" ]]; then
      git add -pi $file
    elif [[ "$control" == "ctrl-r" ]]; then
      git reset HEAD $file
    elif [[ "$control" == "ctrl-e" ]]; then
      $EDITOR $file
    else
      echo $selection
      git status
      return
    fi
  done
}

fda() {
  local cid
  cid=$(docker ps -a | sed 1d | fzf -1 -q "$1" | awk '{print $1}')

  [ -n "$cid" ] && docker start "$cid" && docker attach "$cid"
}

fds() {
  local cid
  cid=$(docker ps | sed 1d | fzf -q "$1" | awk '{print $1}')

  [ -n "$cid" ] && docker stop "$cid"
}

__gitignore() {
  curl -L -s https://www.gitignore.io/api/"$@" |
    sed -n $'/[^\s]/,$p' | # delete empty line
    sed 's/^# Created by .*/& {{{/' | # add fold
    sed 's/^# End of .*/& }}}/'
}

gitignore() {
  if  [ "$#" -eq 0 ]; then
    IFS+=","
    for item in $(__gitignore list); do
      echo $item
    done | fzf --multi --ansi | paste -s -d "," - |
    { read result && __gitignore "$result"; }
  else
    __gitignore "$@"
  fi
}
