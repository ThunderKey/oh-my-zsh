local ret_status="%(?:%{$fg_bold[green]%}➜:%{$fg_bold[red]%}-- %? --
➜%s)"
function rails_env_info_prompt() {
  local info=$(rails_env_info)
  if [ "$info" != "" ]; then
    echo " $info"
  fi
}
function git_stash_prompt() {
  local num_stashed=$(git stash list 2>/dev/null | wc -l)
  if [ "$num_stashed" != "0" ]; then
    echo " [$num_stashed]";
  fi
}
# see https://superuser.com/questions/60555/show-only-current-directory-name-not-full-path-on-bash-prompt#answer-1294418
function show_dirs {
  array=($(echo "${PWD//$HOME/~}" | tr '/' '\n'))
  len=${#array[@]}
  start=$((len - $1 + 1))
  # leading / if fewer dir args: /usr/flastname not usr/flastname
  if (( $start <= 1 )); then
    start=1
    [[ "${array[$start]}" != "~" ]] && echo -n /
  else
    echo -n "[..]/"
  fi
  for (( i = $start; $i <= $len; i++ )); do
    if [[ "${array[$i]}" != "~" ]] && (( $i > $start )); then
      echo -n /
    fi
    echo -n ${array[$i]}
  done
}
PROMPT='${ret_status}%{$fg[cyan]%} $USERNAME@%m:$(show_dirs 3)$(rails_env_info_prompt)%{$fg[cyan]%}%{$fg[red]%}$(git_stash_prompt)%{$fg[cyan]%}> % %{$reset_color%}'
